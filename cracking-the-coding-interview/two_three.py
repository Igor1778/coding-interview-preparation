'''
22/10/19
Solution from CtCI, 15 min
Explained and tested on paper
Sneak peaker too early

Derived to be O(1) time and O(1) space
'''
class two_three(object):
  def delete_node(self,node):
    node.data=node.next_node.data
    node.next_node=node.next_node.next_node
    return