'''
22/09/19
CTCI 16.11
Original solution 40 min
But did it knowing the optimal from the book
'''

class sixteen_eleven(object):
  def all_lengths(self,longer,shorter,k):
    if longer==shorter:
      return [shorter*k]

    result=[]
    for i in range(k+1):
      cur=longer*i +shorter*(k-i)
      result.append(cur)

    return result