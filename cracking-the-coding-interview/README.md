# Cracking the Coding Interview

These are my solutions to a few coding challenges from the book [Cracking the Coding Interview](https://www.amazon.com/Cracking-Coding-Interview-Programming-Questions/dp/0984782850).