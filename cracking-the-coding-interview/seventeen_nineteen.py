class seventeen_nineteen(object):
  '''
  22/09/19
  Original solution to first question and follow up
  Equal to the CtCI question, from my memory
  Everything took 35 min
  Not tested on paper
  '''

  def find_missing_one(self,arr):
    base_sum=0
    for i in range(len(arr)+2):
      base_sum+=i

    arr_sum=0
    for i in range(len(arr)):
      arr_sum+=arr[i]

    return base_sum-arr_sum


  def find_missing_two(self,arr):
    two_sum_diff=self.compute_sum(arr)
    two_prod_diff=self.compute_prod(arr)
    x=self.solve_equation(1,-1*two_sum_diff,two_prod_diff)
    y=two_sum_diff-x
    return (int(x),int(y))

  def solve_equation(a,b,c):
    result=(-1*b + pow((pow(b,2) -4*a*c),1/2))/(2*a)
    return result

  def compute_sum(arr):
    base_sum=0
    for i in range(len(arr)+3):
      base_sum+=i

    arr_sum=0
    for i in range(len(arr)):
      arr_sum+=arr[i]

    return base_sum-arr_sum

  def compute_prod(arr):
    base_prod=1
    for i in range(1,len(arr)+3):
      base_prod*=i

    arr_prod=1
    for i in range(len(arr)):
      arr_prod*=arr[i]

    return base_prod/arr_prod
  
