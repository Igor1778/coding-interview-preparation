'''
19/10/19
Original solution, 35 min
Tested on paper
Derived to be O(nlog(n)) time and O(n) space 
n = number of nodes in the tree
'''

class four_eight(object):
  def first_common_ancestor(self,node1,node2,root):
    if not root:
      return None
    
    one_left=self.search(self,node1,root.left)
    one_right=self.search(self,node1,root.right)
    two_left=self.search(self,node2,root.left)
    two_right=self.search(self,node2,root.right)

    if one_left and two_left:
      return self.first_common_ancestor(self,node1,node2,root.left)
    elif one_right and two_right:
      return self.first_common_ancestor(self,node1,node2,root.right)
    elif one_right and two_left or one_left and two_right:
      return root
    else:
      return None

  def search(self,target,root):
    if not root:
      return False

    if root==target:
      return True

    search_left=self.search(self,target,root.left)
    
    if search_left:
      return True
    
    return self.search(self,target,root.right)

