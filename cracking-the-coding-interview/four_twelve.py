'''
29/09/19
Original solution (remembered from CtCI), 30 min
WRONG ANSWER
'''

class four_twelve(object):
  def paths_with_sum(self,root,target):
    sums=[]
    return self.compute_paths(self,root,target,sums,0)

  def compute_paths(self,node,target,sums,cur):
    if not node:
      return 0

    result=0
    new_cur=cur+node.val
    if new_cur==target:
      result+=1

    for i in range(len(sums)):
      if new_cur-sums[i]==target:
        result+=1

    sums.append(new_cur)
    sums_left,sums_right=sums,sums
    result+=self.compute_paths(self,node.left,target,sums_left,new_cur)
    result+=self.compute_paths(self,node.right,target,sums_right,new_cur)
    return result