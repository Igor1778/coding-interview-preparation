
'''
30/09/19
Original solution, 60 min
I derive it to be O(b*s) time and O(b) space 
b = length of array big
s = length of arrays small
(using a hash table as lookup might be faster, but I ran out of time)
'''

from collections import deque

class seventeen_eighteen(object):
  def shortest_super(self,big,small):
    lookup=self.build_arr_of_queues(big,small)
    result=(0,len(big)-1)
    while deque() not in lookup:
      (lookup_idx,start)=self.get_start_and_lookup_idx(lookup)
      end=self.get_end(lookup)
      if (end-start)<(result[1]-result[0]):
        result=(start,end)

      self.pop_entry(lookup,lookup_idx)

    return result

  def build_arr_of_queues(big,small):
    result=[deque() for i in range(len(small))]

    for i in range(len(big)):
      for j in range(len(small)):
        if big[i]==small[j]:
          result[j].append(i)
          break

    return result

  def get_start_and_lookup_idx(lookup):
    result=(0,lookup[0][0])
    for i in range(len(lookup)):
      if lookup[i][0]<result[1]:
        result=(i,lookup[i][0])

    return result

  def get_end(lookup):
    result=lookup[0][0]
    for i in range(len(lookup)):
      if lookup[i][0]>result:
        result=lookup[i][0]

    return result


  def pop_entry(lookup,pop_idx):
    lookup[pop_idx].popleft()
    return

