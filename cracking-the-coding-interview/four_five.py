'''
29/09/19
Original solution (remembered from CtCI), 30 min
Read from CtCI that it is O(n) time and O(log(n)) space (recursion), where n in shte number of nodes in the tree
'''

class four_five(object):
  def validate_bst(self,root):
    return self.check_nodes(self,root,-1*float('inf'),float('inf'))

  def check_nodes(self,node,min_val,max_val):
    if not node:
      return True
    
    if node.val<=min_val or node.val>=max_val:
      return False

    left=self.check_nodes(self,node.left,min_val,node.val)
    if not left:
      return False

    right=self.check_nodes(self,node.right,node.val,max_val)

    return left and right

    
