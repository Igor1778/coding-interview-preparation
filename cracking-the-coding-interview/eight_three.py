'''
20/10/19
Original solutions, 40 min
Both tested on paper
'''

class eight_three(object):
  '''
  Initial: no duplicates
  Derived to be O(log(n)) time O(1) space 
  n = number of elements in array
  '''
  def magic_index(self,arr):
    left,right=0,len(arr)-1

    while left<=right:
      mid=int((left+right)/2)

      if arr[mid]==mid:
        return mid
      elif arr[mid]<mid:
        left=mid+1
      else:
        right=mid-1

    return -1

  '''
  Follow up: with duplicates
  Derived to be O(n) time (worst case, many duplicates)
  O(n) space (recursive call stack)
  n = number of elements in array
  '''
  def magic_index_2(self,arr):
    return self.helper(self,arr,0,len(arr)-1)

  def helper(self,arr,left,right):
    if left>right:
      return -1

    mid=int((left+right)/2)

    if arr[mid]==mid:
      return mid
    elif arr[mid]<mid:
      check_right=self.helper(self,arr,mid+1,right)
      if check_right>0:
        return check_right
      else:
        return self.helper(self,arr,left,arr[mid])
    else:
      check_left=self.helper(self,arr,left,mid-1)
      if check_left>0:
        return check_left
      else:
        return self.helper(self,arr,arr[mid],right)
    