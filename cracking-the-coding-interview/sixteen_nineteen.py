'''
29/09/19
Original solution, 40 min
Derived to be O(r*c) time and O(r*c) space
Solved or not depending on your solution to the test case I wrote
'''

class sixteen_nineteen(object):
  def pond_sizes(self,grid):
    result=[]
    for i in range(len(grid)):
      for j in range(len(grid[i])):
        if grid[i][j]==0:
          size=self.count_visit(self,grid,i,j,0)
          result.append(size)

    return result
  
  def count_visit(self,grid,row,col,count):
    if row>=len(grid) or row<0 or col>=len(grid[row]) or col<0 or grid[row][col]!=0:
      return 0

    grid[row][col]=-1
    for r_d in range(-1,2):
      for c_d in range(-1,2):
        count+=self.count_visit(self,grid,row+r_d,col+c_d,count)


    count+=1
    return count