'''
20/10/19
Original solution, 35 min
Tested on paper (except for the 'array of length two' bug found here)
Derived to be O(n) time and O(n) space
n = number of elements in array
'''

class seventeen_five(object):
  def largest_subarray(self,arr):
    letters,digits=0,0
    diff=[]

    for i in range(len(arr)):
      if arr[i].isalpha():
        letters+=1
      else:
        digits+=1
      diff.append(letters-digits)

    if len(arr)<=2:
      if digits==1 and letters==1:
        return 2
      else:
        return 0

    lookup=self.build_frq_table(self,diff)
    max_len=0
    for key in lookup:
      cur=lookup[key][len(lookup[key])-1]-lookup[key][0]
      max_len=max(max_len,cur)

    return max_len

  def build_frq_table(self,arr):
    to_return={}

    for i in range(len(arr)):
      key=str(arr[i])
      if key not in to_return:
        to_return[key]=[i]
      else:
        to_return[key].append(i)

    return to_return

      