import heapq
'''
22/09/19
Original solution, 45 min
tested on paper
However, I took a glimpse on CtCI solutions, and had to search for the heap syntax on the web
'''
class seventeen_fourteen(object):
  def get_smallest_k(self,arr,k):
    if k>len(arr) or k<=0:
      return 
    if k==len(arr):
      return arr

    li=[-1*arr[i] for i in range(k)]
    heapq.heapify(li)
    for i in range(k,len(arr)):
      root=-1*li[0]
      if root>arr[i]:
        heapq.heappop(li)
        heapq.heappush(li,-1*arr[i])

    result=self.convert_heap_to_array(li)
    return result

  def convert_heap_to_array(heap):
    to_return=[]
    while heap:
      elem=-1*heapq.heappop(heap)
      to_return.append(elem)

    return to_return