'''
02/10/19
Original solution, 65 min
Not tested on paper, found bugs here
Derived to be O(v^2) time and O(v) space
v = length of value
p = length of pattern 
assuming v>=p

'''

class sixteen_eighteen(object):
  def pattern_matching(self,value,pattern):
    if len(pattern)<=1:
      return True

    length_pairs=self.get_lengths(pattern,len(value))

    if not length_pairs:
      return False

    for pair in length_pairs:
      len_a,len_b = pair[0],pair[1]
      exp_a=self.get_exp(pair,pattern,value,'a')
      exp_b=self.get_exp(pair,pattern,value,'b')
      cur=True
      v_idx,p_idx=0,0
      while p_idx<len(pattern):
        if pattern[p_idx]=='a':
          if value[v_idx:v_idx+len_a]!=exp_a:
            cur=False
          v_idx+=len_a
        else:
          if value[v_idx:v_idx+len_b]!=exp_b:
            cur=False
          v_idx+=len_b
        p_idx+=1
        
      if cur:
        return True

    return False

  def get_lengths(pattern,value_len):
    num_a,num_b=0,0
    for i in range(len(pattern)):
      if pattern[i]=='a':
        num_a+=1
      elif pattern[i]=='b':
        num_b+=1
      else:
        return []

    result=[]
    l_a=0
    while l_a*num_a<=value_len:
      if (value_len-num_a*l_a)%num_b==0:
        l_b=int((value_len-num_a*l_a)/num_b)
        result.append((l_a,l_b))

      l_a+=1

    return result

  def get_exp(pair,pattern,value,char):
    p_idx,v_idx=0,0
    length=pair[0] if char=='a' else pair[1]
    delta=pair[1] if char=='a' else pair[0]
    while pattern[p_idx]!=char:
      p_idx+=1
      v_idx+=delta

    return value[v_idx:v_idx+length]


