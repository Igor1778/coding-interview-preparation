'''
27/09/19
Original, 30 min
Not tested on paper
wrong answer
'''

class eight_eleven(object):
  def num_ways_for_n(self,n):
    coins=[25,10,5,1]
    memo=[-1 for i in range(n+1)]
    return self.num_ways(self,n,coins,memo)

  def num_ways(self,amount,coins,memo):
    if amount<0:
      return 0

    if amount==0:
      return 1

    if memo[amount]!=-1:
      return memo[amount]

    result=0
    for i in range(len(coins)):
      result+=self.num_ways(self,amount-coins[i],coins,memo)

    memo[amount]=result
    return result