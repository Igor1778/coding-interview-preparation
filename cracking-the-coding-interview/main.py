from helpers import SinglyLinkedListNode, TreeNode
from one_two import one_two
from two_three import two_three
from two_five import two_five
from three_four import three_four
from four_five import four_five
from four_eight import four_eight
from four_twelve import four_twelve
from eight_three import eight_three
from eight_eleven import eight_eleven
from sixteen_six import sixteen_six
from sixteen_eleven import sixteen_eleven
from sixteen_sixteen import sixteen_sixteen
from sixteen_eighteen import sixteen_eighteen
from sixteen_nineteen import sixteen_nineteen
from seventeen_five import seventeen_five
from seventeen_fourteen import seventeen_fourteen
from seventeen_sixteen import seventeen_sixteen
from seventeen_eighteen import seventeen_eighteen
from seventeen_nineteen import seventeen_nineteen

print(one_two.is_perm(one_two,"abdaagg","gbdaaag"))

two_three_node_1 = SinglyLinkedListNode('e', None)
two_three_node_2 = SinglyLinkedListNode('d', two_three_node_1)
two_three_node_3 = SinglyLinkedListNode('c', two_three_node_2)
two_three_node_4 = SinglyLinkedListNode('b', two_three_node_3)
two_three_node_5 = SinglyLinkedListNode('a', two_three_node_4)
two_three.delete_node(two_three,two_three_node_3)
two_three_node_cur = two_three_node_5
print("2.3 ->", end=" ")
while two_three_node_cur.next_node:
  print(two_three_node_cur.data, end=" ")
  two_three_node_cur = two_three_node_cur.next_node
print(two_three_node_cur.data)

two_five_node_1 = SinglyLinkedListNode(6, None)
two_five_node_2 = SinglyLinkedListNode(1, two_five_node_1)
two_five_node_3 = SinglyLinkedListNode(7, two_five_node_2)
two_five_node_4 = SinglyLinkedListNode(1, None)
two_five_node_5 = SinglyLinkedListNode(2, two_five_node_4)
two_five_node_6 = SinglyLinkedListNode(9, two_five_node_5)
two_five_node_7 = SinglyLinkedListNode(5, two_five_node_6)
two_five_new_list = two_five.sum_lists(two_five,two_five_node_3, two_five_node_7)
two_five_node_cur = two_five_new_list
print("2.5 ->", end=" ")
while two_five_node_cur.next_node:
  print(two_five_node_cur.data, end=" ")
  two_five_node_cur = two_five_node_cur.next_node
print(two_five_node_cur.data)

three_four.test(three_four)

four_five_node_1 = TreeNode(1,None,None)
four_five_node_3 = TreeNode(3,None,None)
four_five_node_2 = TreeNode(2,four_five_node_1,four_five_node_3)
four_five_node_8 = TreeNode(8,None,None)
four_five_node_9 = TreeNode(9,four_five_node_8,None)
four_five_node_7 = TreeNode(7,None,four_five_node_9)
four_five_root = TreeNode(5,four_five_node_2,four_five_node_7)
print("4.5 ->",four_five.validate_bst(four_five,four_five_root))

four_eight_node_1 = TreeNode(3,None,None)
four_eight_node_2 = TreeNode(7,None,None)
four_eight_node_3 = TreeNode(5,four_eight_node_1,four_eight_node_2)
four_eight_node_4 = TreeNode(2,four_eight_node_3,None)
four_eight_node_5 = TreeNode(8,None,None)
four_eight_node_6 = TreeNode(13,four_eight_node_4,four_eight_node_5)
four_eight_node_7 = TreeNode(5,None,None)
four_eight_node_8 = TreeNode(1,four_eight_node_7,four_eight_node_6)
four_eight_node_9 = TreeNode(17,None,None)
four_eight_node_10 = TreeNode(10,four_eight_node_9,None)
four_eight_root = TreeNode(3,four_eight_node_10,four_eight_node_8)
print("4.8 ->",four_eight.first_common_ancestor(four_eight,four_eight_node_1,four_eight_node_5,four_eight_root).val)

four_twelve_node_minus_3 = TreeNode(-3,None,None)
four_twelve_node_9 = TreeNode(9,None,four_twelve_node_minus_3)
four_twelve_node_2 = TreeNode(-3,four_twelve_node_9,None)
four_twelve_node_minus_1 = TreeNode(-1,None,None)
four_twelve_node_4 = TreeNode(-3,four_twelve_node_2,four_twelve_node_minus_1)
four_twelve_node_3 = TreeNode(3,None,None)
four_twelve_root = TreeNode(5,four_twelve_node_3,four_twelve_node_4)
print("4.12 ->", four_twelve.paths_with_sum(four_twelve,four_twelve_root,8))

print("8.3 ->",eight_three.magic_index_2(eight_three,[-40,-20,2,2,2,3,5,8,9,12,13]))

print("8.11 ->",eight_eleven.num_ways_for_n(eight_eleven,11))

print("16.6 ->",sixteen_six.min_diff(sixteen_six,[1,3,15,11,2],[23,127,235,19,8]))

print("16.11 ->",sixteen_eleven.all_lengths(sixteen_eleven,5,3,7))

print("16.16 ->",sixteen_sixteen.find_range(sixteen_sixteen,[1,2,4,7,10,11,7,12,6,7,16,18,19]))

print("16.18 ->",sixteen_eighteen.pattern_matching(sixteen_eighteen,"catcatgocatgo","aabab"))

grid=[[1,0,1],[0,1,0],[1,0,1]]
print("16.19 ->",sixteen_nineteen.pond_sizes(sixteen_nineteen,grid))

print("17.5 ->",seventeen_five.largest_subarray(seventeen_five,['2','A','f','g','7','15','K','9','0','1','c','Q']))

print("17.14 ->",seventeen_fourteen.get_smallest_k(seventeen_fourteen,[1,4,3,8,7,18,2,5,15,10],4))

print("17.16 ->",seventeen_sixteen.max_minutes(seventeen_sixteen,[30,15,60,57,45,15,15,45]))

print("17.18 ->", seventeen_eighteen.shortest_super(seventeen_eighteen,[7,5,9,0,2,1,3,5,7,9,1,1,5,8,8,9,7],[1,5,9]))

print("17.19 ->",seventeen_nineteen.find_missing_two(seventeen_nineteen,[1,10,5,4,9,8,3,6]))