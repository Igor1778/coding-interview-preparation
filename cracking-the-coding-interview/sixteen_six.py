'''
01/10/19
Original solution, 45 min
I derive it to be O(Blog(B)+ Slog(S)) time and O(1) space
tested on paper
'''

class sixteen_six(object):
  def min_diff(self,arr1,arr2):
    if not arr1 or not arr2:
      return -1

    result=float('inf')
    arr1.sort()
    arr2.sort()
    idx1,idx2 = len(arr1)-1,len(arr2)-1

    while idx1>=0 and idx2>=0:
      diff=abs(arr1[idx1]-arr2[idx2])
      result=min(result,diff)
      if arr1[idx1]>arr2[idx2]:
        idx1-=1
      else:
        idx2-=2

    return result
