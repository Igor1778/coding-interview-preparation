'''
26/10/19
Original solution, 25 min
Tested on paper

Derived to be O(a+b) time and O(u) space

u=number of unique characters in the first string (u<=128 ASCII)
a=length of the first string
b=length of the second string
'''

class one_two(object):
  def is_perm(self,s1,s2):
    if len(s1)!=len(s2):
      return False

    lookup={}
    for i in range(len(s1)):
      if s1[i] not in lookup:
        lookup[s1[i]]=1
      else:
        cur=lookup[s1[i]]
        lookup[s1[i]]=cur+1

    print(lookup)

    for i in range(len(s2)):
      if s2[i] not in lookup:
        return False
      elif lookup[s2[i]]<0:
        return False
      else:
        cur=lookup[s2[i]]
        lookup[s2[i]]=cur-1


    return True
