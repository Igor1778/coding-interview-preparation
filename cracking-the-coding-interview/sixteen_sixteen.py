'''
29/09/19
Original solution (remembered CtCI)
30 min implement + 35 min debug in computer

(problem of including the first/last element or not. It gave the right answer, but it was not inclusive e.g. (3,9) was (2,10))
Derived to be O(n) time and O(1) space
'''

class sixteen_sixteen(object):
  def find_range(self,arr):
    start,end=0,len(arr)-1
    while start<end and arr[start]<=arr[start+1]:
      start+=1

    while end>start and arr[end]>arr[end-1]:
      end-=1

    smallest=self.find_min_or_max(self,arr,start+1,len(arr)-1,True)
    biggest=self.find_min_or_max(self,arr,0,end-1,False)
    while start>0 and arr[start]>smallest:
      start-=1

    if arr[start]<smallest:
      start+=1

    while end<len(arr)-1 and arr[end]<biggest:
      end+=1

    if arr[end]>biggest:
      end-=1

    return (start,end)

  def find_min_or_max(self,arr,idx_1,idx_2,is_min):
    result=float('inf') if is_min else -1*float('inf')
    for i in range(idx_1,idx_2+1):
      if is_min:
        result=min(result,arr[i])
      else:
        result=max(result,arr[i])

    return result

    