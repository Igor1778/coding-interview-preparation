'''
23/10/19
Original solution, 20 min
tested on paper
Considered to be the brute-force in CtCI, can be improved


Derived to be O(n) time and O(n) space
n = maximum number of elements in the queue
'''

class MyQueue(object):
  def __init__(self):
    self.stack1=[]
    self.stack2=[]

  def add(self,element):
    self.stack1.append(element)
    return

  def remove(self):
    while self.stack1:
      elem=self.stack1.pop()
      self.stack2.append(elem)

    self.stack2.pop()

    while self.stack2:
      elem=self.stack2.pop()
      self.stack1.append(elem)

    return

  def print_elements(self):
    print(self.stack1, end=" -> ")
    return

class three_four(object):
  def test(self):
    print("3.4 ->", end=" ")
    queue = MyQueue()
    queue.add(1)
    queue.add(2)
    queue.add(3)
    queue.print_elements()
    queue.remove()
    queue.print_elements()
    queue.add(4)
    queue.print_elements()
    print("done")

    return





  