from helpers import SinglyLinkedListNode
'''
23/10/19
Original solution, 25 min
Tested on paper

Derived to be O(a+b) time and O(max(a,b)+1) space 
a = number of nodes in list1
b = number of nodes in list2
'''
class two_five(object):
  def sum_lists(self,head1,head2):
    node1,node2=head1,head2
    dummy=SinglyLinkedListNode(-1, None)
    cur=dummy
    carry=0

    while node1 or node2:
      val1=node1.data if node1 else 0
      val2=node2.data if node2 else 0
      result=val1+val2+carry
      cur.next_node=SinglyLinkedListNode(result%10, None)
      cur=cur.next_node
      carry=int(result/10)
      if node1:
        node1 = node1.next_node

      if node2:
        node2 = node2.next_node

    if carry:
      cur.next_node=SinglyLinkedListNode(carry, None)

    return dummy.next_node
