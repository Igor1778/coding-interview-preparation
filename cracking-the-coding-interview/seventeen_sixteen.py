class seventeen_sixteen(object):
  '''
  22/09/19
  Solution adapted from CtCI
  I spent 45 min trying to do it but failed because I was not using one_away and two_away as aditional variables
  Reconcile with CtCI algorithm took more 30 min
  Tested on paper
  '''
  def max_minutes(self,arr):
    one_away,two_away=0,0
    for i in range(len(arr)-1,-1,-1):
      best_with=two_away+arr[i]
      best_without=one_away
      cur=max(best_with,best_without)
      two_away=one_away
      one_away=cur

    return one_away

  '''
  26/09/19
  Original solution, 35 min
  Wrong answer
  Not tested on paper
  After this copied all 4 CtCI solutions
  '''
  def max_minutes_slow(self,arr):
    if not arr:
      return 0

    result = 0
    self.schedule_recursive(self,arr,0,0,result)
    return result

  def schedule_recursive(self,arr,index,total,result):
    if index>=len(arr):
      result=max(result,total)
      return

    self.schedule_recursive(self,arr,index+2,total+arr[index],result)
    self.schedule_recursive(self,arr,index+1,total,result)

  '''
  27/09/19
  Fixed in 10 min
  O(2^n) time O(n) space
  '''
  def max_minutes_slow_fixed(self,arr):
    if not arr:
      return 0

    return self.schedule_recursive_fixed(self,arr,0,0)

  def schedule_recursive_fixed(self,arr,index,total):
    if index>=len(arr):
      return total

    best_with = self.schedule_recursive_fixed(self,arr,index+2,total+arr[index])
    best_without = self.schedule_recursive_fixed(self,arr,index+1,total)
    return max(best_with,best_without)

  '''
  27/09/19
  Solutions from CtCI
  Translated to python from memory 25 min
  '''
  '''
  O(2^n) time, O(n) space
  '''
  def max_min_one(self,arr):
    return self.max_min_recursive_one(self,arr,0)

  def max_min_recursive_one(self,arr,idx):
    if idx>=len(arr):
      return 0

    best_with=arr[idx]+self.max_min_recursive_one(self,arr,idx+2)
    best_without=self.max_min_recursive_one(self,arr,idx+1)

    return max(best_with,best_without)

  '''
  O(n) time, O(n) space
  '''
  def max_min_two(self,arr):
    memo=[-1 for i in range(len(arr))]
    return self.max_min_recursive_two(self,arr,0, memo)

  def max_min_recursive_two(self,arr,idx,memo):
    if idx>=len(arr):
      return 0

    if memo[idx]!=-1:
      return memo[idx]

    best_with=arr[idx]+self.max_min_recursive_two(self,arr,idx+2,memo)
    best_without=self.max_min_recursive_two(self,arr,idx+1,memo)

    memo[idx] = max(best_with,best_without)

    return memo[idx]
  '''
  O(n) time, O(n) space
  '''
  def max_min_three(self,arr):
    memo=[0 for i in range(len(arr)+2)]

    for i in range(len(arr)-1,-1,-1):
      best_with=arr[i]+memo[i+2]
      best_without=memo[i+1]
      memo[i]=max(best_with,best_without)

    return memo[0]
  '''
  O(n) time, O(1) space
  '''
  def max_min_four(self,arr):
    one_away,two_away=0,0

    for i in range(len(arr)-1,-1,-1):
      best_with=arr[i]+two_away
      best_without=one_away
      cur=max(best_with,best_without)
      two_away=one_away
      one_away=cur

    return one_away



