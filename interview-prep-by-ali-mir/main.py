from linked_list import Node, LinkedList
from hash_table import HashTable
from array_list import ArrayList
from stack import Stack
from queue import Queue
from binary_search_recursive import BinarySearchRecursiveSolution
from binary_search_iterative import BinarySearchIterativeSolution
from merge_sort import MergeSort
from quick_sort import QuickSort
from graph import Graph, GraphNode, Solution
from bits import Bits
from trie import Trie

class tests(object):
  def test_hash_table():
    table = HashTable(length=6)
    table.add("Igor",21)
    table.add("Peas", 10)
    table.add("Elho", 53)
    table.add("Claudio",54)

    table.remove("Peas")

  def test_array_list():
    array_list=ArrayList()

    array_list.add(1)
    array_list.add(2)
    array_list.add(3)
    array_list.add(4)
    array_list.add(5)
    array_list.add(6)
    array_list.add(7)
    array_list.add(8)
    array_list.add(9)

    array_list.remove()
    array_list.remove()
    array_list.remove()

  def test_stack():
    stack = Stack()

    stack.push(1)
    stack.push(2)
    stack.push(3)
    stack.push(4)
    stack.push(5)
    stack.pop()
    stack.pop()
    stack.pop()

  def test_queue():
    queue = Queue()

    queue.add(1)
    queue.add(2)
    queue.add(3)
    queue.add(4)
    queue.add(5)
    queue.remove()
    queue.remove()
    queue.remove()

  def test_linked_list():
    my_node = Node(data='a')
    my_list = LinkedList(head=my_node)

    my_list.insert('b')
    my_list.insert('c')
    my_list.insert('d')
    my_list.insert('e')

    node = my_list.head

    while node!= None:
      node = node.next_node


do_it = tests

do_it.test_hash_table()
do_it.test_array_list()
do_it.test_stack()
do_it.test_queue()

do_it_2 = BinarySearchRecursiveSolution
print(do_it_2.binary_search_recursive(do_it_2,[-13,-11-6,-1,1,3,4,4,5,7,8,9,12],8))

do_it_3 = BinarySearchIterativeSolution
print(do_it_3.binary_search_iterative(do_it_3,[-13,-11-6,-1,1,3,4,4,5,7,8,9,12],8))

do_it_4 = MergeSort
print(do_it_4.merge_sort(do_it_4,[-1,0,-3,5,8,2,3,2]))

do_it_5 = QuickSort
quick_sort_arr = [-1,0,-3,5,8,2,3,2]
do_it_5.quick_sort(do_it_5,quick_sort_arr)
print(quick_sort_arr)


node_1 = GraphNode(5,[])
node_2 = GraphNode(4,[])
node_3 = GraphNode(2,[])
node_4 = GraphNode(3,[node_3,node_2])
node_5 = GraphNode(1,[node_4,node_2])
node_2.children=[node_5]
node_6 = GraphNode(0,[node_5,node_2,node_1])
graph = Graph([node_1,node_2,node_3,node_4,node_5,node_6])
print(Solution.dfs(Solution,node_6,5), "DFS")
Graph.reset_graph(graph)
print(Solution.bfs(Solution,node_6,5), "BFS")

print(bin(11))
print(bin(Bits.get_ith_bit(11,2)))
print(bin(Bits.set_ith_bit(11,2)))
print(bin(Bits.clear_ith_bit(11,2)))
print(bin(-11))
print(bin(Bits.left_shift_by_i(-11,3)))
print(bin(Bits.arithmetic_right_shift_by_i(-11,3)))
print(bin(Bits.logic_right_shift_by_i(Bits,-11,3)))

trie=Trie()

trie.add_word("many")
trie.add_word("make")
trie.add_word("mold")
trie.add_word("mode")
trie.add_word("manual")
trie.add_word("done")
trie.add_word("dodge")
trie.add_word("mayo")
trie.add_word("doing")
print(trie.contains_word("mold"))
