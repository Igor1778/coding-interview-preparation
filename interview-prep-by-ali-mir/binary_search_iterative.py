'''
16/09/19
Implement binary search iterative
Original, 10 min
'''

class BinarySearchIterativeSolution(object):
  def binary_search_iterative(self,sorted_arr,target):
    left,right=0,len(sorted_arr)-1

    while left<=right:
      mid=int((left+right)/2)
      if sorted_arr[mid]==target:
        return mid
      elif sorted_arr[mid]>target:
        right=mid-1
      else:
        left=mid+1

    return -1