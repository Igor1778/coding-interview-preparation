# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None
'''
20/09/19
Reverse a LinkedList
Original, 20 min, tested manually and in Leetcode
https://leetcode.com/problems/reverse-linked-list/
'''

class Solution(object):
    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if not head:
            return
        
        dummy=ListNode(-1)
        cur_result=dummy
        cur_list=head
        while cur_list.next!=None:
            while cur_list.next.next!=None:
                cur_list=cur_list.next
            cur_result.next=ListNode(cur_list.next.val)
            cur_result=cur_result.next
            cur_list.next=None
            cur_list=head
            
        cur_result.next=ListNode(cur_list.val)
        return dummy.next
        