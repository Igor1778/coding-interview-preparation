import random
'''
18/09/19
Interview prep: implement Quick Sort Algorithm
Original, 40 min
'''

class QuickSort(object):
  def quick_sort(self,arr):
    if len(arr)<=1:
      return arr

    self.helper(self,arr,0,len(arr)-1)

    return arr

  def helper(self,arr,start,end):
    if start>=end:
      return

    idx = random.randint(start,end)
    pivot = arr[idx]
    left,right=start,end
    while left<right:
      if arr[left]<pivot:
        left+=1
      elif arr[right]>pivot:
        right-=1
      else:
        self.swap(arr,left,right)
        left+=1
        right-=1

    mid=int((start+end)/2)
    self.helper(self,arr,start,mid)
    self.helper(self,arr,mid+1,end)
    return
    

  def swap(arr,idx1,idx2):
    tmp=arr[idx1]
    arr[idx1]=arr[idx2]
    arr[idx2]=tmp
    return
    
    