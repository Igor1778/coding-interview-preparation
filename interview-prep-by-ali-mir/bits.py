
'''
21/09/19
Bit manipulation
25 min
original, tested on paper
PYTHON DOES NOT HAVE LOGIC RIGHT SHIFT SO I HAD TO COPY THIS FROM HERE:
https://stackoverflow.com/questions/5832982/how-to-get-the-logical-right-binary-shift-in-python

'''
class Bits(object):
  def get_ith_bit(n,i):
    mask = 1<<i
    return (n & mask) != 0

  def set_ith_bit(n,i):
    mask = 1<<i
    return n | mask

  def clear_ith_bit(n,i):
    mask = 1<<i
    neg_mask = ~mask
    return n & neg_mask

  # Multiplies by 2
  def left_shift_by_i(n,i):
    return n<<i

  # Divides by 2
  def arithmetic_right_shift_by_i(n,i):
    for j in range(i):
      n >>=1

    return n

  def logic_right_shift_by_i(self,n,i):
    for j in range(i):
      n = self.rshift(n,1)

    return n

  def rshift(val, n): return val>>n if val >= 0 else (val+0x100000000)>>n