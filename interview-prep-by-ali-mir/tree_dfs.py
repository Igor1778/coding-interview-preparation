class TreeDFS(object):
  '''
  19/09/19
  Original, 25 min
  Depth First Search Tree Traversals
  Tested manually and in Hackerrank
  https://www.hackerrank.com/challenges/tree-inorder-traversal/problem
  https://www.hackerrank.com/challenges/tree-preorder-traversal/problem
  https://www.hackerrank.com/challenges/tree-postorder-traversal/problem
  '''
  """
  Node is defined as
  self.left (the left child of the node)
  self.right (the right child of the node)
  self.info (the value of the node)
  """
  def preOrder(root):
    #Write your code here
    if not root:
        return

    print(str(root.info)+" ", end="")
    
    if root.left:
        preOrder(root.left)

    if root.right:
        preOrder(root.right)

    return

  def inOrder(root):
    #Write your code here
    if not root:
        return

    if root.left:
        inOrder(root.left)

    print(str(root.info)+" ", end="")

    if root.right:
        inOrder(root.right)

    return

  def postOrder(root):
    #Write your code here
    if not root:
        return

    if root.left:
        postOrder(root.left)

    if root.right:
        postOrder(root.right)

    print(str(root.info)+" ", end="")
    return