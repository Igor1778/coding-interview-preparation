# Interview Preparation by Ali Mir

These are my solutions to the exercises proposed by Ali Mir on [this](https://medium.com/@alimirio/before-you-start-solving-problems-on-leetcode-prep-work-9d65fc964c6f) article from Medium.