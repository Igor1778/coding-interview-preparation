class ArrayList(object):
  '''
  15/09/19
  Implement ArrayList from scratch
  Original, 25 min
  '''
  def __init__(self):
    self.array_list = [None]

  def add(self,n):
    i=0
    while i<len(self.array_list) and self.array_list[i]!=None:
      i+=1

    if i< len(self.array_list):
      self.array_list[i]=n
      return
    else:
      new_arr=self.expand_arr()
      new_arr[i]=n
      self.array_list=new_arr
      return

  def expand_arr(self):
    new_arr=[None for i in range(2*len(self.array_list))]

    for i in range(len(self.array_list)):
      new_arr[i]=self.array_list[i]

    print("expand",self.array_list,new_arr)
    return new_arr

  def remove(self):
    i=len(self.array_list)-1
    while i>=0 and self.array_list[i]==None:
      i-=1

    self.array_list[i]=None

    if i<len(self.array_list)/2:
      new_arr=self.reduce_arr()
      self.array_list=new_arr
    
    return

  def reduce_arr(self):
    new_arr=[]

    for i in range(len(self.array_list)):
      if self.array_list[i]!=None:
        new_arr.append(self.array_list[i])

    print("reduce",self.array_list,new_arr)

    return new_arr