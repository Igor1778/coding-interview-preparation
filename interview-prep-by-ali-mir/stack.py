'''
16/09/19
Implement Stack using tuple
Original, 20 min
'''

class Stack(object):
  def __init__(self):
    self.stack=()

  def push(self, n):
    self.stack=self.stack + (n,)

  def pop(self):
    if self.empty_stack():
      return None
    else:
      length=len(self.stack)
      top=self.stack[length-1]
      self.stack=self.stack[:length-1]
      return top

  def peek(self):
    if self.empty_stack():
      return None
    else:
      length=len(self.stack)
      return self.stack[length-1]

  def empty_stack(self):
    return self.stack==()

  