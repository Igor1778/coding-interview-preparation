class Graph(object):
  def __init__(self,nodes):
    self.nodes=nodes
  
  def reset_graph(graph):
    for node in graph.nodes:
      node.visited=False

class GraphNode(object)    :
  def __init__(self,data,children):
    self.data=data
    self.children=children
    self.visited=False

class Solution(object):
  '''
  20/09/19
  DFS for adjacency lists Graph
  Original, tested here and on paper
  20 min
  '''
  def dfs(self,node,target):
    if not node or node.visited:
      return False

    if node.data==target:
      return True

    node.visited=True
    print(str(node.data) + " ", end="")
    result=False
    children=node.children
    for i in range(len(children)):
      found=self.dfs(self,children[i],target)
      if found:
        result=True

    return result

  '''
  20/09/19
  BFS for adjacency lists Graph
  Original, tested here and on paper
  20 min
  '''
  def bfs(self,node,target):
    if not node or node.visited:
      return False
    print(str(node.data) + " ", end="")
    if node.data==target:
      return True

    result=False
    children=node.children
    for i in range(len(children)):
      print(str(children[i].data) + " ", end="")
      if children[i].data==target:
        return True

    node.visited=True

    for i in range(len(children)):
      found=self.bfs(self,children[i],target)
      if found:
        result=True

    return result
  

    
