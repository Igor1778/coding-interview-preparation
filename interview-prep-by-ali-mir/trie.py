'''
21/09/19
Implement a trie
Original, 80 min
Tested on paper
'''
class TrieNode(object):
  def __init__(self,char,children,terminates):
    self.char=char
    self.children=children
    self.terminates=terminates

  def get_child(self,char):
    result=None
    for i in range(len(self.children)):
      if self.children[i].char==char:
        result=self.children[i]
        break

    return result

class Trie(object):
  def __init__(self):
    self.root=TrieNode('X',[],False)

  def find_last_node_idx_for_word(self,word):
    if not word:
      return
    first=self.root.get_child(word[0])
    if not first:
      return
    result=self.find_last_node_idx_recursive(first,word,0)

    return result

  def find_last_node_idx_recursive(self,node,word,idx):
    if idx>=len(word):
      return None
    
    if node.char!=word[idx]:
      return None

    if idx==len(word)-1:
      return (node,idx)

    result=(node,idx)
    for child in node.children:
      cur=self.find_last_node_idx_recursive(child,word,idx+1)

      if cur:
        result=cur
        break

    return result

  def add_word(self,word):
    print(word)
    result_tuple = self.find_last_node_idx_for_word(word)

    if not result_tuple:
      self.add_word_recursive(self.root,word,0)
    else:
      start_node=result_tuple[0]
      start_idx=result_tuple[1]
      self.add_word_recursive(start_node,word,start_idx+1)

  def add_word_recursive(self,node,word,idx):
    if idx>=len(word):
      return
    new_node=TrieNode(word[idx],[],False)
    if idx==len(word)-1:
      new_node.terminates=True

    node.children.append(new_node)
    self.add_word_recursive(new_node,word,idx+1)

  def contains_word(self,word):
    result = self.find_last_node_idx_for_word(word)

    if not result:
      return False

    idx = result[1]
    if idx!=len(word)-1:
      return False

    return True

    
    

