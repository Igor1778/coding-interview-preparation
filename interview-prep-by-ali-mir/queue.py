'''
16/09/19
Implement Queue using tuple
Original, 10 min
'''

class Queue(object):
  def __init__(self):
    self.queue = ()

  def add(self, n):
    self.queue = (n,) + self.queue
  def remove(self):
    if self.empty_queue():
      return None
    else:
      length=len(self.queue)
      first=self.queue[length-1]
      self.queue=self.queue[:length-1]
      return first

  def peek(self):
    if self.empty_queue():
      return None
    else:
      length=len(self.queue)
      return self.queue[length-1]

  def empty_queue(self):
    return self.queue == ()

