from collections import deque

class Solution(object):
  '''
  19/09/19
  Original, 30 min
  Bredth First Search Tree Traversals
  Tested manually and in Leetcode
  https://leetcode.com/problems/binary-tree-level-order-traversal/
  '''
    def levelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        if not root:
            return
        result=[]
        queue=deque()
        queue.append((root,0))
        
        while queue:
            cur=queue.popleft()
            node=cur[0]
            level=cur[1]
            if level<len(result):
                result[level].append(node.val)
            else:
                result.append([node.val])
            
            if node.left:
                queue.append((node.left,level+1))
                
            if node.right:
                queue.append((node.right,level+1))
                
        return result
        