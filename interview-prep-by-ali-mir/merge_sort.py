'''
18/09/19
Interview prep: implement Merge Sort Algorithm
Original, 35 min
'''

class MergeSort(object):
  def merge_sort(self,arr):
    if len(arr)<=1:
      return arr
    mid=int(len(arr)/2)
    left=self.merge_sort(self,arr[0:mid])
    right=self.merge_sort(self,arr[mid:len(arr)])
    result=[]
    i,j=0,0
    while i<len(left) and j<len(right):
      if left[i]<=right[j]:
        result.append(left[i])
        i+=1
      else:
        result.append(right[j])
        j+=1

    left_over=left[i:len(left)] if i<len(left) else right[j:len(right)]

    for i in range(len(left_over)):
      result.append(left_over[i])

    return result