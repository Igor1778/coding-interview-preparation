'''
16/09/19
Implement binary search recursively
Original, 20 min
'''

class BinarySearchRecursiveSolution(object):
  def binary_search_recursive(self,sorted_arr,n):
    return self.search(self,0,len(sorted_arr)-1,sorted_arr,n)

  def search(self,left,right,arr,target):
    
    if left>right:
      return -1

    mid=int((left+right)/2)

    if arr[mid]==target:
      return mid
    elif arr[mid]>target:
      return self.search(self,left,mid-1,arr,target)
    else:
      return self.search(self,mid+1,right,arr,target)