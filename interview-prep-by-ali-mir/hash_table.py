from linked_list import Node, LinkedList

'''
15/09/19
Implement Hash Table with simple hashing functions
Original, 60 min
'''

class HashTable(object):
  def __init__(self,length=1):
    self.length=length
    self.table=[None for i in range(length)]

  def get_index(self,key):
    hash_code=0
    for i in range(len(key)):
      value=int(ord(key[i]))
      hash_code+=value

    print("Hash Code",key,hash_code, (hash_code % self.length))
    return hash_code % self.length

  def add(self,key,value):
    print("added "+key+" "+str(value))
    index=self.get_index(key)
    if self.table[index]==None:
      self.table[index]=LinkedList(head=Node(key=key,value=value))
      return
    else:
      self.handle_collision(index,key,value)
    

  def handle_collision(self,index,key,value):
    node=self.table[index].head
    while node.next_node!=None:
      print(node.key,node.value)
      node=node.next_node

    node.next_node=Node(key=key,value=value)
    return

  def get(self,key):
    index=self.get_index(key)
    if self.table[index]==None:
      return None
    else:
      node=self.find_node_for_key(index,key)
      if node==None:
        return None
      else:
        return node.value

  def find_node_for_key(self,index,key):
    node=self.table[index].head
    while node!=None and node.key!=key:
      node=node.next_node

    return node

  def remove(self,key):
    print("removed value of "+key)
    index=self.get_index(key)
    node=self.table[index].head
    if node.key==key:
      self.table[index].head=node.next_node
      return

    while node.next_node!=None and node.next_node.key!=key:
      node=node.next_node

    if node.next_node!=None:
      node.next_node=None

    return
