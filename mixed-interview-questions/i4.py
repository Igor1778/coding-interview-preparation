from collections import deque
class i4(object):
  '''
  10/09/19
  i4 - Google Coding Sample Summer 2019 SWE Intern

  There are n guests who are invited to a party. The k-th guest will attend the party at time S[k] and leave the party at time E[k].

  Given an integer array S and an integer array E, both of length n, return an integer denoting the minimum number of chairs you need such that everyone attending the party can sit down.


  1<=n<=500
  Elements of S and E are within the range 1-1000
  S[k]<E[k] for every k within the range 1-n


  Original solution, 40 min.
  Works for the test case given.
  I derive it to be O(nlog(n)) time and O(n) space, where n is the number of elements in one of the arrays.
  '''

  def count_chairs(S,E):
    E.sort()
    S.sort()
    count, departures = 1, deque()
    departures.append(E[0])
    for i in range(1,len(S)):
      if S[i] < departures[0]:
        count+=1
      else:
        departures.popleft()

      departures.append(E[i])
      

    return count

