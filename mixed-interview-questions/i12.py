class i12(object):
  '''
  04/09/19
  i12 - No Pairs Allowed
  Hackerrank question from Twitter SWE Intern 2018/2019

  Original "solution", about 40 min
  I think it is O(ab) time and O(ab) space 
  (where a is the number of words and b is the number of characters in the longest word)
  '''

  def no_pairs_allowed(words):
    result = [0 for i in range(len(words))]

    for i in range(len(words)):
      count=0
      j=0
      while j<(len(words[i])-1):
        if words[i][j] == words[i][j+1]:
          count+=1
          j+=2
        j+=1
      result[i]=count
    
    return result