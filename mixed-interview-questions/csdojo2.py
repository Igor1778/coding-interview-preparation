import math
import heapq
'''
15/10/19 about 20 min
Solution from CSDojo from memory
https://www.youtube.com/watch?v=eaYX0Ee0Kcg

I guessed it to be O(n(log(k))) time and O(n) space.
However, after watching the video now I know that it is O(n + (n-k)log(k)) time and O(n) space
'''
class csdojo2(object):
  def k_closest_points(self,points,k):
    points_dist=[]
    for point in points:
      dist=math.sqrt(pow(point[0],2)+pow(point[1],2))
      points_dist.append((-1*dist,point)) #negative distance to be a max heap

    heap = []
    for i in range(k):
      heap.append(points_dist[i])

    heapq.heapify(heap)

    for i in range(k, len(points_dist)):
      top=heap[0]
      if top[0]<points_dist[i][0]:
        heapq.heappop(heap)
        heapq.heappush(heap,points_dist[i])

    return self.heap_to_points_arr(heap)


  def heap_to_points_arr(heap):
    to_return=[]

    while heap:
      point_dist= heapq.heappop(heap)
      to_return.append(point_dist[1])

    return to_return

    
