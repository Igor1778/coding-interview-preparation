class i8(object):
  '''
  07/09/19
  i8 - Google Phone Interview Fall 2019

  You are given two strings A and B (consisting of lowercase English alphabets). Your aim is form string A 
  by concatenating multiple copies of string B. However, before concatenating a copy of string B, you can 
  remove any number of characters from it.

  For example,
  A = 'ZAZA' 
  B = 'BAZ'
  You can form string A by concatenating three copies of string B as follows:
  '##Z' + '#AZ' + '#A#' (# denotes the characters removed from string B).

   Write a method to count the minimum number of operations you'd have to do to form string A out of edited copies of string B. If it is not possible, return -1.

  Original attempt, 50 min.
  Works for the test case given, but need more testing.
  I derive it to be O(log(a)+2^b) time and O(log(a)+2^b) space, where 'a' is the length of string A and 'b' is the length of string B.
  '''
  def min_operations(self, s1, s2):
    subs = self.get_sorted_subs_by_size(self,s2)
    return self.min_ops_for_words(self,s1,subs,0)

  def min_ops_for_words(self,cur,subs,count):
    if cur=='':
      return count 
    to_return=-1
    for i in range(len(subs)):
      end_prefix=self.find_prefix_end_idx(self,cur,subs[i])
      if end_prefix!=-1:
        to_return=self.min_ops_for_words(self,cur[end_prefix:],subs,count+1)
        break
    return to_return

  def find_prefix_end_idx(self,s,prefix):
    if len(prefix)>len(s):
      return -1
    i=0
    while i<len(prefix):
      if prefix[i]!=s[i]:
        return-1
      i+=1
    return i

  def get_sorted_subs_by_size(self, s):
    to_return = []
    self.subs_by_size_recurse(self,s,0,'',to_return)
    to_return.sort(key=len, reverse=True)
    return to_return

  def subs_by_size_recurse(self,s,idx,cur,subs):
    if idx>=len(s):
      subs.append(cur)
      return
    with_char = cur + s[idx]
    without_char = cur
    self.subs_by_size_recurse(self,s,idx+1,with_char,subs)
    self.subs_by_size_recurse(self,s,idx+1,without_char,subs)



