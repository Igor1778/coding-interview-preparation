class i1(object):
  '''
  06/09/19
  i1 - Find pairs summing to target
  Microsoft SWE Intern phone screening 2019

  Given a group of numbers and a target value,
  write a function to return all paris of numbers that exactly add up to the target. There might be duplicate
  numbers in the group, and you can use them as many times
  as they appear on the list, but your return must nothave duplicate pairs, and e.g. [1,3] and [3,1] are considered
  to be the same pair. The group has only integers and
  is not sorted.You can return the pairs in whatever data structure you'd like.

  I: group = [1,5,7,-1] target = 6
  O: [[1,5],[7,-1]]


  Original Solution, 35 min
  I derive it as O(n) time and O(n) space
  (n is the number of integers in the group)
  '''

  def find_pairs(self,arr,target):
    result=[]
    num_set=self.build_set(arr)
    even_target = target%2==0
    has_two_halves = False
    if even_target:
      has_two_halves = self.check_halves(arr,target/2)
    
    if even_target and not has_two_halves:
      num_set.discard(target/2)
    
    for i in range(len(arr)):
      if target-arr[i] in num_set:
        result.append([arr[i],target-arr[i]])
        num_set.discard(arr[i])
        num_set.discard(target-arr[i])

    return result
  
  def check_halves(arr,num):
    seen=False
    for i in range(len(arr)):
      if arr[i]==num:
        if seen:
          return True
        seen = True

    return False

  def build_set(arr):
    result=set()
    for i in range(len(arr)):
      result.add(arr[i])

    return result