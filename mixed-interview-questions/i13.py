class i13(object):
  '''
  13/09/19
  i13 - Twitter University Recruiting Coding Challenge 2018/2019 - Braces (Hackerrank)

  Given an array A of strings, check if the braces are check_balanced.
  - All braces should be closed: [] () {}
  - In any set of nested braces, the braces between any pair must be closed e.g. ({}) is balanced but ({)} is not.
  - 1<=n<=15
  - 1<=length of A[i]<=100
  - A[i] is composed only of (, {, [, ], } and ).

  Original solution 60 min
  took additional 20 min because of unsuccessful attempt of making it O(1) space.
  I derive it to be O(n*l) time and O(l) space, where
  n is the number of strings in the array and l is the number of characters of the longest of theses strings
  '''
  def check_balanced_strings(self,arr):
    to_return = []
    for s in arr:
      result = self.check_balanced(s)
      to_return.append(result)

    return to_return

  def check_balanced(s):
    stack = []
    left = set(['(','{','['])
    for i in range(len(s)):
      if s[i] in left:
        stack.append(s[i])
      else:
        top = stack.pop()
        if s[i]==')' and top!='(' or s[i]=='}' and top!='{' or s[i]==']' and top!='[':
          return "NO"

    return "YES" if not stack else "NO"