# Mixed Interview Problems

These are the solutions to a few problems I found interesting during my studies. They are:

- Problems from [CS Dojo](https://www.youtube.com/channel/UCxX9wt5FWQUAAz4UrysqK9A) videos;
- Problems from [Lintcode](https://www.lintcode.com/), which has all the problems from Leetcode Premium for free. You can search for them by their names using [this](https://github.com/kongpingfan/Leetcode-Premium) list.
- Problems I have been given on real interviews (over the phone or on site) for companies such as `Google`, `Microsoft`, `Twitter`, `Flexport` and others;