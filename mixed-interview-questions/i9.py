class i9(object):
  '''
  i9 - Question from Phone Screening with a SWE from Google for a SWE intern role - 19/07/19

  Q1: Minimum flips to make all 1s in right and 0s in left
  Given an array of 0 and 1, each time we can flip 0 to 1 or 1 to 0, what is the minimum number of flips needed to make the array have all 0s in the left and 1s in the right?

  original: 001010
  goal:      001111 -> 2
                000111 -> 3
                000011 -> 2

  Solution made in 26/07/19 from my head:
  '''
  def find_min_flips(self, arr):
    min_flips = len(arr)

    for i in range(len(arr)+1):
      left = arr[0:i]
      right = arr[i:len(arr)]
      cur = self.get_frequency(self,left,1) + self.get_frequency(self,right,0)
      min_flips = min(min_flips,cur)
    
    return min_flips

  def get_frequency(self,arr,num):
    count = 0

    for i in range(len(arr)):
      if arr[i] == num:
        count += 1
    
    return count