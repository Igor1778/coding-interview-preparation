class i6(object):
  '''
  21/09/19
  i6 - Google Coding Sample Fall 2019 SWE Intern
  Array x is greater than array y if the first non matching element in both arrays has a greater value in x than y. For example, if x=[1,2,4,3,5] and y=[1,2,3,4,5], x is greater than y because x[2]>y[2]. A contigouous subarray is a subarray which has consecutive indexes. Write a function that, given a zero-indexed array A consisting of N integers and an integer K, returns the largest contigouous subarray of length K from all the contiguous subarrays of length K.

  - 1<=<=KN<=100;
  - 1<=A[j]<=1000;
  - Focus on correctness

  Input: A=[1,4,3,2,5] K=4
  Output: [4,3,2,5]

  Original solution, 30 min
  I derive it to be O(nk) time and O(k) space, where:
  k = input k given
  n = number of elements in the array given
  '''

  def biggest_subarray(self,arr,k):
    if k==len(arr):
      return arr
    
    start,end=0,k
    result=arr[start:end]
    while end<=len(arr):
      is_bigger=self.compare(self,arr[start:end],result)
      if is_bigger:
        result=arr[start:end]
      start+=1
      end+=1
    
    return result

  def compare(self,arr1,arr2):
    idx=0
    while idx<len(arr1) and arr1[idx]==arr2[idx]:
      idx+=1

    if idx>=len(arr1):
      return False

    return arr1[idx]>arr2[idx]