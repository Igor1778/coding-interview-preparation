class i11(object):
  '''
  12/09/19
  i11 - Flexport Phone Interview Fall 2019 - Follow up
  Implement a function that, given a string composed of digits 0-9 concatenated and a dictionary that maps from a digit (in string format) to an array of characters, returns an array with all the possible strings one could get from decoding the original string. 
  E.g.
  
  Input: digits='12' 
  wordDict = {'1':['a','b'],'2':['c','d']}

  Output: ['ac','ad','bc','bd']

  [Answered in i10]

  Follow up: now the scenario could be the follows. Any contiguous substring of digits is a potential key. Return an array considering all the possibilities of keys.

  Input: digits='12' 
  wordDict = {'1':['a','b'],'2':['c','d'], '12':['x']}

  Output: ['ac','ad','bc','bd', 'x']

  Original solution, 25 min.
  I derived bigO to be O(n!*n^2) time (because you are making an O(1) operation for every expansion of key in every index) and O(n!) space (there seem to be n recursive calls at each tree level (to reduce 1 character in digits) each of which could expand to n other calls).
  n = number of characters in string to decode
  '''


  def possible_decodes(self, s, wordDict):
    result = []
    self.decode_recursive(self,s,wordDict,result,0,'')
    return result

  def decode_recursive(self,s,wordDict,result,idx,cur):
    if idx >= len(s):
      result.append(cur)
      return

    for i in range(idx,len(s)):
      key = s[idx:i+1]
      if key in wordDict:
        for char in wordDict[key]:
          new_cur = cur + char
          self.decode_recursive(self,s,wordDict,result,i+1,new_cur)

    return