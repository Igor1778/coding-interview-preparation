class i10(object):
  '''
  08/09/19
  i10 - Flexport Phone Interview Fall 2019
  Implement a function that, given a string composed of digits 0-9 concatenated and a dictionary that maps from a digit (in string format) to an array of characters, returns an array with all the possible strings one could get from decoding the original string. 
  E.g.
  
  Input: digits='12' 
  wordDict = {'1':['a','b'],'2':['c','d']}

  Output: ['ac','ad','bc','bd']


  Original solution, 35 min.
  I derived bigO to be O(n) time and O(n) space.
  n = number of characters in string to decode
  '''

  def possible_decodes(self,s,wordDict):
    result = []
    self.decode_recursive(self,s,wordDict,result,0,'')
    return result
  
  def decode_recursive(self,s,wordDict,decoded,idx,cur):
    if idx>=len(s):
      decoded.append(cur)
      return

    key=s[idx]

    if key not in wordDict:
      return

    for char in wordDict[key]:
      new_cur=cur+char
      self.decode_recursive(self,s,wordDict,decoded,idx+1,new_cur)