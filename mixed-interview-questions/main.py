from i1 import i1
from i2 import i2
from i4 import i4
from i5 import i5
from i6 import i6
from i7 import i7
from i8 import i8
from i9 import i9
from i10 import i10
from i11 import i11
from i12 import i12
from i13 import i13
from csdojo1 import csdojo1
from csdojo2 import csdojo2
from csdojo3 import csdojo3
from lintcode_540 import lintcode_540
from techlead1 import techlead1


print(i1.find_pairs(i1,[1,3,2,5,9,8,3,7,13],10), "expected [[1, 9],[3, 7],[2, 8]]")

print(i2.format_license(i2,'2-4A0r7-4k',3), "expected 24-A0r-74k")

print(i4.count_chairs([1, 2, 6, 5, 3], [5, 5, 7, 6, 8]), "expected 3")

print(i5.count_smaller_str(i5,'abcd,aabc,bd','aaa,aa'), 'expected [3,2]')

print(i6.biggest_subarray(i6,[1,4,3,2,4,5,7,3,5],2))

print(i7.can_form('zaza','baz'), "expected True")

print(i8.min_operations(i8,'zaza','baz'), "expected 3")

print(i9.find_min_flips(i9,[0,0,1,0,1,0]),"expected 2")

print(i11.possible_decodes(i10,'12',{'1':['a','b'],'2':['c','d']}), "expected ['ac', 'ad', 'bc', 'bd']")

print(i10.possible_decodes(i11,'112',{'1':['a','b'],'2':['c','d'], '12':['x'], '112':['f','g']}), "expected ['aac', 'aad', 'abc', 'abd', 'ax', 'bac', 'bad', 'bbc', 'bbd','bx', 'f', 'g']")

print(i12.no_pairs_allowed(['add','boook','break','gooooodss']), "expected [1, 1, 0, 3]")

print(i13.check_balanced_strings(i13, ['{([])()}', '({)','({(())}', '(){}[]']), "expected ['YES', 'NO', 'NO', 'YES']")

print(lintcode_540.zig_zag(lintcode_540,[1,2],[3,4,5,6]))

grid=[[1,1,1,0,0],[1,0,0,2,2],[0,0,3,0,3],[2,0,3,3,3]]
print(techlead1.find_max_color(techlead1,grid))

print(csdojo1.num_ways(csdojo1,'27345'))

print(csdojo2.k_closest_points(csdojo2, [(1,2),(0,-2),(1,1),(-3,3),(-1,0)],2))

print(csdojo3.closest_pair_3(csdojo3,[0,11,8,14,4,30],[6,5,10,2,7,18],25))

