'''
26/10/19
Original "solution", 20 min

Was supposed to be for Lintcode 540. Zigzag Iterator problem, but I did not know I should have written a class

Derived to be O(a+b) time and O(a+b) space 
a = length of array 1
b = length of array 2
'''

class lintcode_540(object):
  def zig_zag(self,arr1,arr2):
    result=[]
    idx1,idx2=0,0
    while idx1<len(arr1) and idx2<len(arr2):
      if idx1<=idx2:
        result.append(arr1[idx1])
        idx1+=1
      else:
        result.append(arr2[idx2])
        idx2+=1

    if idx1==len(arr1):
      self.copy_remaining(self,result,arr2,idx2)
    else:
      self.copy_remaining(self,result,arr1,idx1)

    return result

  def copy_remaining(self,result,arr,idx):
    for i in range(idx,len(arr)):
      result.append(arr[i])

    return result
