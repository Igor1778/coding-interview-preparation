# from collections import deque

'''
12/10/19
<https://www.youtube.com/watch?v=IWvbPIYQPFM>
Original solution, about 30 min
Inspired by techlead talk
Derived to be O(n) time and O(n) space where n is the number of cells in the grid
'''
'''
Given a grid, find the maximum number of connected colors.
'''

class techlead1(object):
  def find_max_color(self,grid):
    result=0
    lookup={}
    for i in range(len(grid)):
      for j in range(len(grid[i])):
        key=str(i)+'-'+str(j)
        if grid[i][j]!=0 and key not in lookup:
          cur=self.dfs_recursive(self,grid,i,j,lookup)
          # cur=self.dfs_iterative(self,grid,i,j,lookup)
          result=max(result,cur)

    return result

  '''
  13/10/19
  dfs iterative, about 15 min
  Original, just remember Techlead suggestions
  Overall algorithm derived to be O(n) time and O(n) space but now no longer limited by stack overflow
  '''
  '''
  def dfs_iterative(self,grid,row,col,lookup):
    result=0
    key=str(row)+'-'+str(col)
    lookup[key]=True
    queue=deque()
    queue.append((row,col))

    while queue:
      cur=queue.popleft()
      result+=1
      neighbors=self.get_neighbors(self,grid,cur[0],cur[1],lookup)
      for neighbor in neighbors:
        queue.append(neighbor)

    return result
  '''

  def dfs_recursive(self,grid,row,col,lookup):
    result=1
    key=str(row)+'-'+str(col)
    lookup[key]=True

    neighbors=self.get_neighbors(self,grid,row,col,lookup)
    for neighbor in neighbors:
      result+=self.dfs_recursive(self,grid,neighbor[0],neighbor[1],lookup)

    return result

  def get_neighbors(self,grid,row,col,lookup):
    result=[]
    for dr in range(-1,2):
      for dc in range(-1,2):
        key=str(row+dr)+'-'+str(col+dc)
        if row+dr>0 and row+dr<len(grid) and col+dc>0 and col+dc<len(grid[row]) and grid[row+dr][col+dc]==grid[row][col] and key not in lookup:
          result.append((row+dr,col+dc))
          lookup[key]=True

    return result
