from collections import deque

'''
18/10/19
Google SWE Intern USA - Coding Sample Questions - Summer 2019

You are given S, composed of N characters, and an integer K. S is composed of alphanumberical characters and/or dashes. Dashes split the characters in groups (if there are M dashes, there are M+1 groups) and they might be misplaced. Write a function that, given a non empty string S and an integer K, returns a string (formatted license key) whose groups have length K (maybe except for the first group that might be shorter but must have at least one character)

- N and K are in the range 0-300k;
- String S is composed of a-z and/or A-Z and/or 0-9 characters and/or dashes;
- String S has at least one character


Original solution, 25 min
Tested on paper
Derived to be O(n) time and O(n) space
n = number of characters in license
'''


class i2(object):
  def format_license(self, license, k):
    queue=deque()
    result=""

    for i in range(len(license)):
      if license[i]!='-':
        queue.append(license[i])

    if len(queue) <= 1 or len(queue) <= k:
      while queue:
        result+=queue.popleft()

      return result

    offset=len(queue)%k

    if offset!=0:
      for i in range(offset):
        char=queue.popleft()
        result+=char
      result+='-'

    while queue:
      for i in range(k):
        result+=queue.popleft()
      
      if queue:
        result+='-'

    return result
      

      