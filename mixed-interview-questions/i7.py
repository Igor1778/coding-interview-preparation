class i7(object):
  '''
  07/09/19
  i7 - Google Phone Interview Fall 2019 
  You are given two strings A and B (consisting of lowercase English alphabets). Your aim is form string A 
  by concatenating multiple copies of string B. However, before concatenating a copy of string B, you can 
  remove any number of characters from it.

  For example,
  A = 'ZAZA' 
  B = 'BAZ'
  You can form string A by concatenating three copies of string B as follows:
  '##Z' + '#AZ' + '#A#' (# denotes the characters removed from string B).

  Write a method that returns True if it is possible to form string A by concatenating edited copies of string B, otherwise returns False. What is the big O time/space complexity?

  Original solution, 35 min.
  I derived bigO to be O(a+b) time and O(b) space.
  a = number of characters in string A
  b = number of characters in string B
  '''
  def can_form(s1,s2):
    if s1 == '':
      return True
    if s1 != '' and s2 == '':
      return False

    s2_set = set()

    for i in range(len(s2)):
      s2_set.add(s2[i])

    for i in range(len(s1)):
      if s1[i] not in s2_set:
        return False

    return True