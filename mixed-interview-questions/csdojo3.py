'''
15/10/19 50 min
Solutions from CSDojo video from memory, did not check any resource.
https://www.youtube.com/watch?v=GBuHSRDGZBY 
Each one tested with two testcases on PC
'''

class csdojo3(object):
  '''
  O(n^2) time and O(1) space 
  n=number of elements in one of the arrays
  '''
  def closest_pair_1(self,arr1,arr2,target):
    closest=abs(arr1[0]+arr2[0]-target)
    result=(arr1[0],arr2[0])

    for i in range(len(arr1)):
      for j in range(len(arr2)):
        if abs(arr1[i]+arr2[j]-target)<closest:
          closest=abs(arr1[i]+arr2[j]-target)
          result=(arr1[i],arr2[j])

    return result

  '''
  O(xn) time and O(n) space 
  n=number of elements in one of the arrays
  x=number of traverses made in arr1
  '''
  def closest_pair_2(self,arr1,arr2,target):
    result=(-1,-1)
    lookup=set(arr2)
    low,high=target,target
    found=False
    while not found:
      for i in range(len(arr1)):
        if (low-arr1[i]) in lookup:
          result=(arr1[i],low-arr1[i])
          found=True
          break

        if (high-arr1[i]) in lookup:
          result=(arr1[i],high-arr1[i])
          found=True
          break
      low-=1
      high+=1

    return result

  '''
  O(nlog(n)) time and O(1) space 
  n=number of elements in one of the arrays
  '''
  def closest_pair_3(self,arr1,arr2,target):
    arr1.sort()
    arr2.sort()
    idx1=len(arr1)-1
    idx2=0
    closest=abs(arr1[idx1]+arr2[idx2]-target)
    result=(arr1[idx1],arr2[idx2])

    while idx1>=0 and idx2<len(arr2):
      cur=arr1[idx1]+arr2[idx2]
      if abs(cur-target)<closest:
        closest=abs(cur-target)
        result=(arr1[idx1],arr2[idx2])
      if cur<target:
        idx2+=1
      else:
        idx1-=1

    return result


        

