class i5(object):
  '''
  13/09/19
  i5 - Google Coding Sample Fall 2019 SWE Intern
  Ones string is strictly smaller than another when the frequency of occurrence of the smallest character in the string is less than the freuency of occurrence of the smallest character in the comparison string. For example, 'abcd' is smaller than 'aaa', and 'a' is smaller than 'bb'. Write a function that given two strings A and B, both composed of N strings delimited by ',', returns an array C of N integers. for 0<=J<N,  C[J] represents the number of strings in A that are strictly smaller than the comparisonm Jth string in B, starting from 0.

  - 1<=N<=10000;
  - 1<=length of strings in A and B<=10;
  - A and B contains only lowercase alphabet letters a-z;
  - Focus on correctness

  Input: A = 'abcd,aabc,bd' B ='aaa,aa'
  Output: C = [3,2] because aa>abcd, aa>bd and aaa is bigger than all the others

  Original solution, 45 min
  I derive it to be O(Na*Nb +LaNa + LbNb) time and O(Na+Nb) space, where:
  La = number of characters the longest string in A
  Na = number of comma-separated strings in A
  Lb = number of characters the longest string in B
  Nb = number of comma-separated strings in B

  '''
  def count_smaller_str(self,s_a,s_b):
    arr_a = self.get_arr_of_str(s_a)
    arr_b = self.get_arr_of_str(s_b)
    ranking_a = self.get_ranking(arr_a)
    ranking_b = self.get_ranking(arr_b)
    result = []

    for entry in ranking_b:
      count = self.count_smaller_than(entry,ranking_a)
      result.append(count)

    return result

  def get_ranking(arr):
    result = []
    for s in arr:
      smallest_char = 'z'
      count = 0
      for i in range(len(s)):
        if s[i] < smallest_char:
          smallest_char = s[i]

      for i in range(len(s)):
        if s[i] == smallest_char:
          count += 1

      result.append(count)
    
    return result

  def count_smaller_than(n,arr):
    count = 0
    for i in range(len(arr)):
      if arr[i] < n:
        count +=1

    return count

  def get_arr_of_str(s):
    result = []
    cur, i = '', 0

    while  i < len(s):
      if s[i] == ',':
        result.append(cur)
        cur = ''
        i += 1

      if i == len(s)-1:
        cur += s[i]
        result.append(cur)
        break
      
      cur += s[i]
      i += 1

    return result
'''
You should know that the following code does the same:

  def get_arr_of_str(s):
    return s.split(',')

'''

