'''
13/10/19
Original solution, 45 min
Derived to be O(n) time and O(n) space 
n is the number of characters in the string 
Started with the non-memoized O(2^n) time and O(n) space solution
'''
class csdojo1(object):
  def num_ways(self,data):
    memo=[-1 for i in range(len(data)+1)]
    return self.helper(self,data,len(data),memo)

  def helper(self,data,k,memo):
    if k==0:
      return 1
    
    start=len(data)-k

    if int(data[start])==0: #important to have int here
      return 0

    if k==1:
      return 1

    if memo[k]!=-1:
      return memo[k]

    result=0
    result+=self.helper(self,data,k-1,memo)
    if k>=2 and int(data[start:start+2])<=26:
      result+=self.helper(self,data,k-2,memo)

    memo[k]=result
    return result
